
class Modal {
    constructor() {
        this.card = 'https://ajax.test-danit.com/api/cards';
        this.login = 'https://ajax.test-danit.com/api/cards/login';
    }

    getCardInfo() {
            return fetch(this.card,{
                method: 'GET',
                headers: {
                     Authorization: `Bearer ${'d7cb498c-a0ae-4c18-a855-e4e4e01b6f48'}`,
                    'Content-Type': 'application/json',
                },
            })
        }
    getLogin() {
        return fetch(this.login, {
            method: 'POST',
            body: JSON.stringify({
                email: "your.mail@gmail.com",
                password: "12344344аа"
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        })
    }

    createPopup() {
        const popupBtn           = document.querySelector('.enter-btn');
        const pageNoItemMessage  = document.createElement('p');
        const modalBg            = document.querySelector('.modal-bg');
        const modalClose         = document.querySelector('.modal-close');

        pageNoItemMessage.textContent = 'Please LogIn.';
        pageNoItemMessage.style.cssText = 'font-weight: bold; font-size: 40px; text-align: center;';
        document.body.append(pageNoItemMessage);

        popupBtn.addEventListener('click', () => {
            modalBg.classList.add('bg-active');
            pageNoItemMessage.textContent = '';
        });

        modalClose.addEventListener('click', () => {
            modalBg.classList.remove('bg-active');
            pageNoItemMessage.textContent = '';
        });
    }

    signInTransition() {

        const signInBtn = document.querySelector('.sign_in_btn');
        const createCardBtn = document.querySelector('.create_card_btn')
        const modalBg = document.querySelector('.modal-bg');
        const pageNoItemMessage  = document.createElement('p');
        const popupBtn = document.querySelector('.modal-btn');
        const searchForm = document.querySelector('.second_block')
        const cardPlace = document.querySelector('.place_for_cards')


        signInBtn.addEventListener('click', (e) => {
            e.preventDefault()
            createCardBtn.classList.remove('hidden');
            modalBg.classList.remove('bg-active');
            pageNoItemMessage.textContent = '';
            popupBtn.style.cssText = 'display: none;';
            searchForm.classList.remove('hidden');
            cardPlace.style.cssText = 'display: flex'
        })

    }
}

const modal = new Modal();

modal.createPopup();
modal.signInTransition()
modal.getLogin()
    .then(res =>
        console.log(res)
    );

class Form {
    constructor() {

    }

    createCard() {
        const createNewCardBtn = document.querySelector('.create_card_btn');
        const template = document.querySelector('.my-template');
        const select = document.querySelector('.select-doctor');
        const therapist = document.querySelector('.therapist');
        const cardiologist = document.querySelector('.cardiologist');
        const dentist = document.querySelector('.dentist');
        const card = document.querySelector('.card');
        // const lastVisitForTherapist = document.querySelector('.last-visit');
        // const lastVisitForTherapistInput = document.querySelector('.last-visit-input');
        dentist.classList.add('hidden');
        therapist.classList.add('hidden');
        cardiologist.classList.add('hidden');

        createNewCardBtn.addEventListener('click', () => {
            template.classList.add('hidden-block');
        });
        select.addEventListener('click', function() {
            if (this.value === 'Стоматолог')
            {
                dentist.classList.remove('hidden');
                card.style.height = '700px';
                therapist.classList.add('hidden');
                cardiologist.classList.add('hidden');
            }
            else if (this.value === 'Терапевт')
            {
                therapist.classList.remove('hidden');
                dentist.classList.add('hidden');
                cardiologist.classList.add('hidden');
                card.style.height = '600px';
                // lastVisitForTherapist.classList.toggle('hidden');
                // lastVisitForTherapistInput.classList.toggle('hidden');
            }
            else if (this.value === 'Кардиолог') {
                dentist.classList.add('hidden');
                therapist.classList.add('hidden');
                cardiologist.classList.remove('hidden')

                card.style.height = '1040px';
                // lastVisitForTherapist.classList.toggle('hidden');
                // lastVisitForTherapistInput.classList.toggle('hidden');
            }
        });


    }
    chooseDoctor() {

    }
}
const form = new Form();
console.log(form.createCard());